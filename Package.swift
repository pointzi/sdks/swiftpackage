// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PointziSDK",
    products: [
        .library(
            name: "StreetHawkCore_Pointzi",
            targets: ["StreetHawkCore_Pointzi"]),
    ],
    targets: [
        .binaryTarget(
           name: "StreetHawkCore_Pointzi",
            url:"https://repo.pointzi.com/sdk/pointzi/ios/spm/2.11.4/StreetHawkCore_Pointzi.zip",
           checksum: "dc0bb56c83866a6a2f2471dc74e3d80e033e332691f36b29f1f195772c33ffa2")
    ]
)
