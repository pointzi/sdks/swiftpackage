# PointziSDK
This repository contains Swift Package Manger implementation of Pointzi SDK 


## Prerequisites
- Deployment target iOS 13.0 and above.
- Latest Xcode.


This setup assumes you have already setup your App Key on the Dashboard.

## Step 1 - SDK Installation & Configuration 

### Add Packages
In Xcode, select File > Swift Packages > Add Package Dependency and enter the repository URL
- https://gitlab.com/pointzi/sdks/swiftpackage.git
- https://gitlab.com/pointzi/sdks/pointzispmwrapper

![After Adding Packages](/images/after_adding_packages.png) 
### Add Linker Flags
Go to Project -> Build Settings -> Other Linker flags and Add the following 
>
> -ObjC
>

![Linker Settings](/images/linker_settings.png) 

### Adding the Resources
- Download the Pointzi bundle from https://repo.pointzi.com/artifactory/pointzi/ios/spm/Pointzi.bundle.zip
- Unzip and Drag and Drop Pointzi.bundle into your project
![Add bundle](/images/add_bundle.png) 

## Step 2 - Initialization

In your Application launch function, add pointzi registration.

If you are using Swift, Add the following code in AppDelegate

```swift 
import StreetHawkCore_Pointz
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Replace 'MyApp' with your app_key, registered in the Pointzi Dashboard.
        Pointzi.sharedInstance().registerInstall(forApp: "MyApp", withDebugMode: false)
        // Set 'withDebugMode' to 'true' to enable debug logging.
        return true
    }
}
```


If you are using Objective-C,  Add the following code in AppDelegate 
```objc
#import "StreetHawkCore_Pointzi.h"
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Replace 'MyApp' with your app_key, registered in the Pointzi Dashboard.
    [POINTZI registerInstallForApp:@"MyApp" withDebugMode:NO];
    // Set 'withDebugMode' to 'YES' to enable debug logging.
    return YES;
}
```

## Step 3 - Replace Base Classes

Replace the base ViewController classes and inherit from Pointzi SDK classes as shown below.


|From                      |To                                    |
|--------------------------|--------------------------------------|
|UITableViewController     |StreetHawkBaseTableViewController     |
|UIViewController          |StreetHawkBaseViewController          |
|UICollectionViewController|StreetHawkBaseCollectionViewController|

For React Native, In your AppDelegate.m , Replace UIViewController with StreetHawkBaseViewController
**Objc**
```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //Replace UIViewController with StreetHawkBaseViewController
  UIViewController *rootViewController = [StreetHawkBaseViewController new];
}
```

>
>  If your view controller overrides system functions, make sure you call ```super```.
>

>
>  Replacing your view controller with the Pointzi controller **is** essential for Pointzi to function correctly.
>  Please ensure that if you have multiple view controllers showing on the same screen to only replace the parent (top ViewController).
>
## Step 4 - Set sh_cuid [Optional]

We recommend you also send a unique ID for the user so you can "tag" users from your backend system. This is called the sh_cuid.

If you would like to tag the user during registration just place the following snippet.

**Swift**
```swift
//unique id in your system, for example customer's login id
Pointzi.sharedInstance().registerInstall(forApp: "MyApp", withDebugMode: false) {
    Pointzi.sharedInstance().tagCuid("user@example.com")
}
```

**Objc**
```objc
//unique id in your system, for example customer's login id
[POINTZI registerInstallForApp:@"MyApp" withDebugMode:NO completion:^{
    [POINTZI tagCuid:@"user@example.com"];
}];
```

If you would like to tag the user at any other place in your application eg after login call, just place the following snippet.

**Swift**
``` swift
//unique id in your system, for example customer's login id
   Pointzi.sharedInstance().tagCuid("user@example.com")
```

**Objc**
``` objc
//unique id in your system, for example customer's login id
    [POINTZI tagCuid:@"user@example.com"];
```
